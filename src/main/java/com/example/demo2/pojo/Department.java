package com.example.demo2.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//物品种类表
@Data
@AllArgsConstructor//有参构造器
@NoArgsConstructor//无参构造器
public class Department {
    private Integer id;
    private String name;
}
