package com.example.demo2.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

//商品类
@Data
@AllArgsConstructor//全参构造器
@NoArgsConstructor//无参构造器
public class Product {
    private Integer id;//商品ID
    private String name;//商品名称
    private Integer num;//商品数量
    private Integer price;//商品单价
    private String unit;//计量单位
    private String prodecedate;//生产日期或者到货日期
    //如果需要添加日期的话，可以考虑去掉有参构造器，然后自己写一个构造器，自动创建日期，这样子构造时就能少一个参数

    private Department department;//商品所属种类，例如苹果属于水果

}
