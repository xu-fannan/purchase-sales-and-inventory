package com.example.demo2.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
public class LoginController {

    @RequestMapping("/user/login")
    public String login(@RequestParam("username") String username,
                        @RequestParam("password") String password,
                        Model model, HttpSession session)
    {
        //具体业务
        if (StringUtils.hasText(username)&&password.equals("hit")){
            session.setAttribute("loginUser",username);
            return "redirect:/main.html"; //登录成功后，重定向至main.html,即实际的index，这样就能简单隐藏账号密码信息
        }else{
            //告诉用户，登录失败
            model.addAttribute("msg","用户名或密码错误！");
            return "login";
        }
    }

    //实现注销功能
    @RequestMapping("/user/logout")
    public String logout(HttpSession session){
        session.invalidate();
        return "redirect:/login";
    }
}
