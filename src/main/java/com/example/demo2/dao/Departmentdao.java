package com.example.demo2.dao;

import com.example.demo2.pojo.Department;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

//物品种类表dao
@Repository
public class Departmentdao {
    //模拟数据库中的数据
    private static Map<Integer, Department> departmentMap=null;
    static {
        departmentMap=new HashMap<Integer,Department>();//创建一个物品种类表表
        departmentMap.put(101,new Department(101,"水果"));
        departmentMap.put(102,new Department(102,"蔬菜"));
        departmentMap.put(103,new Department(103,"牛奶"));
        departmentMap.put(104,new Department(104,"饮料"));
    }
    //获取所有物品种类表信息
    public Collection<Department> getDepartments(){
        return departmentMap.values();
    }
    //通过id得到信息
    public Department getDepartmentById(Integer id){
        return departmentMap.get(id);
    }
}
