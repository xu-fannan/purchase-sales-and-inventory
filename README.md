# 进销存

#### 介绍
前端页面及相关静态资源套用Bootstrap开源模板进行修改，初步实现了登录注销，登录拦截器，商品的增删改查功能。
#### 软件架构
软件架构说明


#### 安装教程

1.使用IDEA打开项目

#### 使用说明

1. 启动Demo1Application中的main方法
2. 等待Tomcat部署完成
3. 浏览器输入localhost:8080/login进入登录界面(输入localhost:8080/……之类的也没关系，最后都会跳转到登录页面)
4. 用户名随意，密码为hit即可登录系统
5. 请点击库存表管理商品（因为其他页面还没有对应具体的业务，但也可以点，不过会造成用户信息的丢失）


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
